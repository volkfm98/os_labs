#include "stdafx.h"

#include <windows.h>
#include <windowsx.h>
#include <stdlib.h>
#include <tchar.h>
#include <process.h>

#include <vector>
#include <fstream>
#include <algorithm>
#include <string>

int N = 0;

int WinHeight = 240;
int WinWidth = 320;

COLORREF bgColor = RGB(0, 0, 255);
COLORREF netColor = RGB(255, 0, 0);

char* iconName;

char* args;

char* circles;

bool isCircle = false;

HANDLE hBGThread;

struct Texture {
	char* data;
	int width, height;
};

Texture circleTexture, crossTexture;

bool ansiStrEq(char* a, char* b) {
	int i;
	for (i = 0; a[i] != '\0' && b[i] != '\0' && a[i] == b[i]; ++i);
	return a[i] == b[i];
}

int random(int n0, int n1) {
	if (n0 == 0 && n1 == 1) return rand() % 2;
	return rand() % (n1 - n0) + n0;
}

void ApplyCfg(const char* filePtr) {
	filePtr += 7;

	if (*filePtr == '\n') {
		filePtr++;
	}

	filePtr += 2;

	N = 0;
	WinWidth = 0;
	WinHeight = 0;

	for (; *filePtr != '\n' && *filePtr != '\r'; filePtr++) {
		N *= 10;
		N += *filePtr - '0';
	}

	filePtr++;

	if (*filePtr == '\n') {
		filePtr++;
	}

	filePtr += 9;

	for (; *filePtr != '\n' && *filePtr != '\r'; filePtr++) {
		WinWidth *= 10;
		WinWidth += *filePtr - '0';
	}

	filePtr++;

	if (*filePtr == '\n') {
		filePtr++;
	}

	filePtr += 10;

	for (; *filePtr != '\n' && *filePtr != '\r'; filePtr++) {
		WinHeight *= 10;
		WinHeight += *filePtr - '0';
	}

	filePtr++;

	if (*filePtr == '\n') {
		filePtr++;
	}

	filePtr += 8;

	int r = 0, g = 0, b = 0;

	for (; *filePtr != ','; filePtr++) {
		r *= 10;
		r += *filePtr - '0';
	}

	filePtr++;

	for (; *filePtr != ','; filePtr++) {
		g *= 10;
		g += *filePtr - '0';
	}

	filePtr++;

	for (; *filePtr != '\n' && *filePtr != '\r'; filePtr++) {
		b *= 10;
		b += *filePtr - '0';
	}

	bgColor = RGB(r, g, b);

	filePtr++;

	if (*filePtr == '\n') {
		filePtr++;
	}

	filePtr += 9;

	r = 0;
	g = 0;
	b = 0;

	for (; *filePtr != ','; filePtr++) {
		r *= 10;
		r += *filePtr - '0';
	}

	filePtr++;

	for (; *filePtr != ','; filePtr++) {
		g *= 10;
		g += *filePtr - '0';
	}

	filePtr++;

	for (; *filePtr != '\n' && *filePtr != '\r'; filePtr++) {
		b *= 10;
		b += *filePtr - '0';
	}

	netColor = RGB(r, g, b);

	filePtr++;

	if (*filePtr == '\n') {
		filePtr++;
	}

	filePtr += 9;

	iconName = _strdup(filePtr);
}

const char* GetCfg(HWND hWnd) {
	std::string data;
	
	LPRECT cliRect = new RECT();

	GetWindowRect(hWnd, cliRect);

	data = "[sec1]\nN=";
	data += std::to_string(N);

	data += "\nWinWidth=";
	data += std::to_string(cliRect->right - cliRect->left);

	data += "\nWinHeight=";
	data += std::to_string(cliRect->bottom - cliRect->top);

	LOGBRUSH logBrush;
	GetObject((HANDLE)GetClassLongPtr(hWnd, GCLP_HBRBACKGROUND), sizeof(LOGBRUSH), &logBrush);

	bgColor = logBrush.lbColor;

	data += "\nbgColor=";
	data += std::to_string(GetRValue(bgColor));
	data += ',';
	data += std::to_string(GetGValue(bgColor));
	data += ',';
	data += std::to_string(GetBValue(bgColor));
	
	data += "\nnetColor=";
	data += std::to_string(GetRValue(netColor));
	data += ',';
	data += std::to_string(GetGValue(netColor));
	data += ',';
	data += std::to_string(GetBValue(netColor));

	data += "\niconName=";
	data += iconName;

	DeleteObject(cliRect);

	return _strdup(data.c_str());
}

void LoadCfgMem() {
	HANDLE hFile = CreateFile(_T("cfg.ini"),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	HANDLE hMapObj = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);

	char* filePtr = (char*)MapViewOfFile(hMapObj, FILE_MAP_READ, 0, 0, 0);
	void* basePtr = (void*)filePtr;
	
	ApplyCfg(filePtr);

	UnmapViewOfFile(basePtr);
	CloseHandle(hMapObj);
	CloseHandle(hFile);
}
void LoadCfgWinApi() {
	HANDLE hFile = CreateFile(_T("cfg.ini"),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	LPDWORD fileSize = new DWORD();
	LPDWORD bytesWrittenCount = new DWORD();

	*fileSize = GetFileSize(hFile, NULL);

	char* filePtr = (char*)malloc((int)(*fileSize) + 1);
	char* tmp = filePtr;

	if (!ReadFile(hFile, filePtr, *fileSize, bytesWrittenCount, NULL)) {
		DWORD err = GetLastError();
		return;
	}

	filePtr[*fileSize] = '\0';

	ApplyCfg(filePtr);
	//BUGFIX: delete raises error 
	delete tmp;
	delete bytesWrittenCount;
	delete fileSize;

	CloseHandle(hFile);
}
void LoadCfgStream() {
	std::ifstream in("cfg.ini");

	std::string str((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	
	ApplyCfg((char*)str.c_str());
}
void LoadCfgFile() {
	FILE* file;
	fopen_s(&file, "cfg.ini", "rb");

	fseek(file, 0, SEEK_END);

	int size = ftell(file);

	fseek(file, 0, SEEK_SET);

	char* content = (char*)malloc(size + 1);

	fread((void*)content, sizeof(char), size + 1, file);
	content[size] = '\0';

	ApplyCfg(content);

	fclose(file);
}

void SaveCfgMem(const char* content) {
	HANDLE hFile = CreateFile(_T("cfg.ini"),
		GENERIC_READ | GENERIC_WRITE,
		NULL,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	HANDLE hMapObj = CreateFileMapping(hFile, NULL, PAGE_READWRITE, 0, 0, NULL);

	DWORD err = GetLastError();

	char* filePtr = (char*)MapViewOfFile(hMapObj, FILE_MAP_WRITE, 0, 0, 0);

	memcpy(filePtr, content, sizeof(char) * strlen(content));

	UnmapViewOfFile(filePtr);
	CloseHandle(hMapObj);
	CloseHandle(hFile);
}
void SaveCfgWinApi(const char* content) {
	HANDLE hFile = CreateFile(_T("cfg.ini"),
		GENERIC_READ | GENERIC_WRITE,
		NULL,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	LPDWORD sz = new DWORD();

	if (!WriteFile(hFile, content, sizeof(char) * strlen(content), sz, NULL)) {
		DWORD err = GetLastError();
	}

	delete sz;

	CloseHandle(hFile);
}
void SaveCfgStream(const char* content) {
	std::ofstream file("cfg.ini");

	file << content;

	file.close();
}
void SaveCfgFile(const char* content) {
	FILE* file;
	fopen_s(&file, "cfg.ini", "wb");

	fwrite(content, sizeof(char), strlen(content), file);

	fclose(file);
}

int LoadCfg(char* args) {
	if (ansiStrEq(args, (char*)"memory\0")) {
		LoadCfgMem();
	} else

	if (ansiStrEq(args, (char*)"files\0")) {
		LoadCfgFile();
	} else

	if (ansiStrEq(args, (char*)"stream\0")) {
		LoadCfgStream();
	} else

	//if (ansiStrEq(args, (char*)"winapi\0")) {
		LoadCfgWinApi();
	//} else {
	//	return 0;
	//}

	return 1;
}
int SaveCfg(char* args, HWND hWnd) {
	const char* content = GetCfg(hWnd);

	if (ansiStrEq(args, (char*)"memory\0")) {
		SaveCfgMem(content);
	} else

	if (ansiStrEq(args, (char*)"files\0")) {
		SaveCfgFile(content);
	} else

	if (ansiStrEq(args, (char*)"stream\0")) {
		SaveCfgStream(content);
	} else

	if (ansiStrEq(args, (char*)"winapi\0")) {
		SaveCfgWinApi(content);
	} else {
		return 0;
	}

	return 1;
}

void loadTextures() {
	HMODULE libHandle = LoadLibrary(_T("C:\\Users\\volkf\\source\\repos\\WinAPI-Desktop\\x64\\Debug\\MOVS_Img_loader.dll"));

	char* (*loadImage)(const char*	filename, int*	width, int*	height) =
		(char* (*)(const char*	filename, int*	width, int*	height))GetProcAddress(libHandle, "load_image");
	
	circleTexture.data = loadImage("circle.bmp", &circleTexture.width, &circleTexture.height);
	crossTexture.data = loadImage("cross.png", &crossTexture.width, &crossTexture.height);

	crossTexture.width = 32;
	crossTexture.height = 32;

	FreeLibrary(libHandle);
}

DWORD changeBGColor(void* param) {
	//hBGThread = GetCurrentThread();

	HWND hWnd = (HWND)param;

	HBRUSH bgBrush = GetSysColorBrush(COLOR_BACKGROUND);

	LOGBRUSH logBrush;
	while (1) {
		GetObject((HANDLE)GetClassLongPtr(hWnd, GCLP_HBRBACKGROUND), sizeof(LOGBRUSH), &logBrush);

		bgColor = logBrush.lbColor;

		for (int i = 0; i < 256; i += 10) {
			bgBrush = GetSysColorBrush(COLOR_BACKGROUND);
			DeleteObject(bgBrush);

			bgBrush = CreateSolidBrush(RGB(255 - i, 255, 255));
			Sleep(200);

			SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG)bgBrush);

			InvalidateRect(hWnd, NULL, true);
		}

		for (int i = 0; i < 256; i += 10) {
			bgBrush = GetSysColorBrush(COLOR_BACKGROUND);
			DeleteObject(bgBrush);

			bgBrush = CreateSolidBrush(RGB(255, 255 - i, 255));
			Sleep(200);

			SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG)bgBrush);

			InvalidateRect(hWnd, NULL, true);
		}

		for (int i = 0; i < 256; i += 10) {
			bgBrush = GetSysColorBrush(COLOR_BACKGROUND);
			DeleteObject(bgBrush);

			bgBrush = CreateSolidBrush(RGB(255, 255, 255 - i));
			Sleep(200);

			SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG)bgBrush);

InvalidateRect(hWnd, NULL, true);
		}
	}
}

HANDLE createSharedMem() {
	long long int sz = N * N + 2 * sizeof(HWND) + sizeof(bool);
	HANDLE hSharedMem = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, HIWORD(sz), LOWORD(sz), _T("Global\\field"));

	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		isCircle = true;
	} else {
		isCircle = false;
	}

	circles = (char*)MapViewOfFile(hSharedMem, FILE_MAP_ALL_ACCESS, 0, 0, sz);

	return hSharedMem;
}

HBITMAP hBitmap;

void checkGameState(int player, int x, int y) {
	int i;
	
	for (i = 0; i < N; ++i) {
		if (circles[i * N + y] != player) {
			break;
		}
	}

	if (i != N) {
		for (i = 0; i < N; ++i) {
			if (circles[x * N + i] != player) {
				break;
			}
		}
	}

	if (i == N) {
		if (player == 1) {
			MessageBox(NULL, _T("Game over! Player1 wins!"), _T("Tic|Cross"), MB_OK | MB_SETFOREGROUND);
		} else {
			MessageBox(NULL, _T("Game over! Player2 wins!"), _T("Tic|Cross"), MB_OK | MB_SETFOREGROUND);
		}

		SendMessage(*((HWND*)(circles + N * N)), WM_CLOSE, 0, 0);
		SendMessage(*((HWND*)(circles + N * N + sizeof(HWND))), WM_CLOSE, 0, 0);
	}
}

LRESULT CALLBACK WndProc(
	_In_ HWND hWnd,
	_In_ UINT umsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
) {
	PAINTSTRUCT ps;
	HDC hdc;

	LPRECT rcRedraw = new RECT();

	switch (umsg) {
	case WM_PAINT: {
		hdc = BeginPaint(hWnd, &ps);

		SelectObject(hdc, GetStockObject(DC_PEN));
		SetDCPenColor(hdc, netColor);

		SelectObject(hdc, GetStockObject(DC_BRUSH));
		SetDCBrushColor(hdc, RGB(0, 255, 0));

		if (N != 0) {
			for (int i = (WinWidth / N); i < WinWidth; i += (WinWidth / N)) {
				MoveToEx(hdc, i, 0, NULL);
				LineTo(hdc, i, WinHeight);
			}

			for (int i = (WinHeight / N); i < WinHeight; i += (WinHeight / N)) {
				MoveToEx(hdc, 0, i, NULL);
				LineTo(hdc, WinWidth, i);
			}
		}

		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				if (circles[i * N + j] != 0) {
					HDC hdcSrc = CreateCompatibleDC(hdc);
					HBITMAP hBitmap;
					HBITMAP oldBmp;

					if (circles[i * N + j] == 2) {
						hBitmap = CreateBitmap(crossTexture.width, crossTexture.height, 1, 32, crossTexture.data);

						oldBmp = (HBITMAP)SelectObject(hdcSrc, hBitmap);

						BitBlt(hdc,
							i * (WinWidth / N), j * (WinHeight / N),
							crossTexture.width, crossTexture.height,
							hdcSrc,
							0, 0,
							SRCCOPY);
					} else {
						hBitmap = CreateBitmap(circleTexture.width, circleTexture.height, 1, 32, circleTexture.data);

						oldBmp = (HBITMAP)SelectObject(hdcSrc, hBitmap);

						BitBlt(hdc,
							i * (WinWidth / N), j * (WinHeight / N),
							circleTexture.width, circleTexture.height,
							hdcSrc,
							0, 0,
							SRCCOPY);
					}

					DeleteObject(hdcSrc);
					DeleteObject(oldBmp);
					DeleteBitmap(hBitmap);
				}
			}
		}

		EndPaint(hWnd, &ps);
		DeleteObject(hdc);

		break;
	}

	case WM_LBUTTONDOWN: {
		if (isCircle && *((bool*)(circles + N * N + 2 * sizeof(HWND))) || !isCircle && !*((bool*)(circles + N * N + 2 * sizeof(HWND)))) {
			MessageBox(NULL, _T("It's not your turn."), _T("Tic|Cross"), MB_OK | MB_SETFOREGROUND);
		} else {
			if (N != 0) {
				int xPos = GET_X_LPARAM(lParam) / (WinWidth / N);
				int yPos = GET_Y_LPARAM(lParam) / (WinHeight / N);

				if (xPos < N && yPos < N) {
					if (circles[xPos * N + yPos] != 0) {
						MessageBox(NULL, _T("You can't do it here."), _T("Tic|Cross"), MB_OK | MB_SETFOREGROUND);
					} else {
						if (isCircle) {
							circles[xPos * N + yPos] = 1;

							checkGameState(1, xPos, yPos);
						} else {
							circles[xPos * N + yPos] = 2;
							checkGameState(2, xPos, yPos);
						}

						*((bool*)(circles + N * N + 2 * sizeof(HWND))) = !*((bool*)(circles + N * N + 2 * sizeof(HWND)));

						xPos *= (WinWidth / N);
						yPos *= (WinHeight / N);

						DeleteObject(rcRedraw);
						SetRect(rcRedraw, xPos, yPos, xPos + (WinWidth / N), yPos + (WinHeight / N));

						InvalidateRect(*((HWND*)(circles + N * N)), rcRedraw, true);
						InvalidateRect(*((HWND*)(circles + N * N + sizeof(HWND))), rcRedraw, true);
					}
				}
			}
		}

		break;
	}

	case WM_KEYUP: {
		switch (wParam) {
		case VK_SPACE: {
			if (SuspendThread(hBGThread)) {
				ResumeThread(hBGThread);
				ResumeThread(hBGThread);
			}

			break;
		}

		case 0x30:
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			break;
		case 0x31:
			SetPriorityClass(GetCurrentProcess(), PROCESS_MODE_BACKGROUND_BEGIN);
			break;
		case 0x32:
			SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS);
			break;
		case 0x33:
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			break;

		case VK_ESCAPE: {
			PostQuitMessage(0);
			break;
		}
		}

		break;
	}

	case WM_CLOSE: {
		PostQuitMessage(0);
		break;
	}

	case WM_HOTKEY: {
		switch (lParam) {
		case 0x430004: {
			WinExec("notepad", SW_SHOW);
			break;
		}

		case 0x510002: {
			PostQuitMessage(0);
			break;
		}

		case 0x530002: {
			SaveCfg(args, hWnd);
		}
		}

		break;
	}

	case WM_QUIT: {
		DeleteObject(rcRedraw);
		break;
	}

	default: {
		return DefWindowProc(hWnd, umsg, wParam, lParam);
		break;
	}
	}

	return 0;
}

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine,
	_In_ int nCmdShow
) {
	loadTextures();

	args = lpCmdLine;
	LoadCfg(args);

	HANDLE hSharedMem = createSharedMem();

	WNDCLASSEX wcex;

	static TCHAR szWindowClass[] = _T("DesktopApp");
	static TCHAR szTitle[] = _T("Windows Desktop Guided Tour Application");

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCWSTR)iconName);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = CreateSolidBrush(bgColor);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

	if (!RegisterClassEx(&wcex)) {
		MessageBox(NULL,
			_T("What a shame! We couldnt register the class"),
			_T("Error"),
			NULL);
		return 1;
	}

	HWND hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		WinWidth, WinHeight,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	if (!hWnd)
	{
		MessageBox(NULL,
			_T("Call to CreateWindow failed!"),
			_T("Windows Desktop Guided Tour"),
			NULL);

		return 1;
	}

	RegisterHotKey(hWnd, 1, MOD_SHIFT, 0x43);
	RegisterHotKey(hWnd, 2, MOD_CONTROL, 0x51);
	RegisterHotKey(hWnd, 1, MOD_CONTROL, 0x53);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	if ((HWND)(*(circles + N * N)) == NULL) {
		*((HWND*)(circles + N * N)) = hWnd;
		*((bool*)(circles + N * N + 2 * sizeof(HWND))) = true;
	} else {
		if ((HWND)(*(circles + N * N + sizeof(HWND))) == NULL) {
			*((HWND*)(circles + N * N + sizeof(HWND))) = hWnd;
		} else {
			PostQuitMessage(0);
		}
	}

	hBGThread = CreateThread(NULL, 0, changeBGColor, (void*)hWnd, 0, NULL);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	UnmapViewOfFile(circles);
	CloseHandle(hSharedMem);

	DestroyWindow(hWnd);
	UnregisterClass(_T("wcex"), hInstance);

	return (int)msg.wParam;
}